Ingredienti Impasto

- 300gr farina 0
- 200gr farina tipo manitoba
- 3gr lievito secco
- 1 cucchiaino di zucchero
- 400ml di acqua
- 1 cucchiaio di olio extra vergine
- 11gr di sale

Ingredienti pizza

- 1 scatola di Polpa di pomodoro 
- 1/2 scatola di Pelati (opzionale)
- 1 Mozzarella
- 1 cucchiaio di Olio extra vergine
- Basilico 
- Origano

Step 1 - Pre fermemto

- Miscelare le farine
- Conservare 100gr di farina mista per lo Step 2
- Conservare 1,5gr di lievito per lo Step 2
- Aggiungere 1,5gr di lievito secco alle farine
- Aggiungere lo zucchero
- Aggiungere acqua e amalgamare con un cucchiaio
- Verifica che l'impasto finale sia molle e appiccicoso
- Coprire l'impasto con una pellicola
- Segnare l'altezza dell'impasto, dovrà triplicare
- Far livitare dalle 6h alle 8h a 22 gradi


Step 2

- L'impasto sarà molle e triplicato con bollicine in superficie
- Miscelare le farine e lievito rimanenti
- Versare sul piano la farina
- Aggiungere il prefermento con un raschietto di mettallo o un tarocco
- Aggiungere olio e sale
- Amalgamare ripiegando l'impasto per 10 min
- Spolverare di farina il panetto
- Lievitare per 1h

Step 3

- Preriscaldare il forno a 240 gradi
- Unire il pomodoro,origano e aggiungere un filo d’olio
- Disporre l’impasto su una teglia ricoperta con carta forno
- Allargare l’impasto con polpastrelli dal centro all’esterno fino a ricoprire tutta la superficie
- Aggiungere il pomodoro
- Cuocere per 10minuti 
- Tagliare la mozzarella 
- Aggiungere mozzarella
- Cuocere per 9min
